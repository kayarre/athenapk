variables:
  CMAKE_VER: '3.20.3'
  HDF5_VER: '1.10.7'
  OMPI_VER: '4.1.0'
  REBUILD_DEP: 'FALSE'

# Is performed before the scripts in the stages step
before_script:
  - git submodule init
  - git submodule foreach git reset --hard
  - git submodule update
  - export OMP_PROC_BIND=close
  - export OMP_PLACES=cores
  - export OMP_NUM_THREADS=1
  - export CTEST_OUTPUT_ON_FAILURE=1
  - export J=8 && echo Using ${J} cores during build
  - export PATH=/home/build/athenapk/cmake-${CMAKE_VER}-linux-x86_64/bin:$PATH
  - export OMPI_DIR=/home/build/athenapk/openmpi-${OMPI_VER}
  - export OPAL_PREFIX=/home/build/athenapk/openmpi-${OMPI_VER}
  - export PATH=${OMPI_DIR}/bin:$PATH
  - export LD_LIBRARY_PATH=${OMPI_DIR}/lib:$LD_LIBRARY_PATH
  - export KOKKOS_NUM_DEVICES=1
  - export CUDA_VISIBLE_DEVICES=0
# trying to use a less frequently used GPU on the node
  - module load anaconda3/2020.02
  # required as functions like "activate" are not available in subshells, see
  # see https://github.com/conda/conda/issues/7980
  - source /opt/sns/anaconda3/2020.02/etc/profile.d/conda.sh

stages:
  - prep
  - format
  - regression

format:
  stage: format
  tags:
    - gpu
    - springdale7
  script:
    - pip install clang-format
    # using this cumbersome workaround as clang-format 9 shipped with pip does not support -Werror yet
    - for fname in $(find tst src -iname \*.cpp -o -iname \*.hpp); do diff -u $fname <(/home/build/.local/bin/clang-format -style=file $fname); done

prep-cuda:
  stage: prep
  tags:
    - gpu
    - springdale7
  script:
    - module load rh/devtoolset/8 cudatoolkit/11.1
    # for rebuilding, the pipeline should be triggered manually with REBUILD_DEP=TRUE
    - if [ $REBUILD_DEP == TRUE ]; then
      rm -rf /home/build/athenapk/cmake-${CMAKE_VER}-linux-x86_64;
      rm -rf /home/build/athenapk/openmpi-${OMPI_VER};
      rm -rf /home/build/athenapk/hdf5-${HDF5_VER}-gcc;
      fi
    - if [ ! -d /home/build/athenapk/cmake-${CMAKE_VER}-linux-x86_64 ]; then
      mkdir -p /home/build/athenapk;
      cd /home/build/athenapk;
      wget -qO- http://www.cmake.org/files/v${CMAKE_VER:0:4}/cmake-${CMAKE_VER}-linux-x86_64.tar.gz | tar -xz;
      fi
    - if [ ! -d /home/build/athenapk/openmpi-${OMPI_VER} ]; then
      mkdir -p /home/build/athenapk/openmpi-${OMPI_VER};
      TMPDIR=$(mktemp -d);
      cd $TMPDIR;
      wget https://download.open-mpi.org/release/open-mpi/v4.1/openmpi-${OMPI_VER}.tar.bz2;
      tar xjf openmpi-${OMPI_VER}.tar.bz2;
      cd openmpi-${OMPI_VER};
      ./configure --prefix=/home/build/athenapk/openmpi-${OMPI_VER} --disable-mpi-fortran --with-cuda --without-ucx --without-ofi && make all install;
      cd;
      rm -rf $TMPDIR;
      fi
    - if [ ! -d /home/build/athenapk/hdf5-${HDF5_VER}-gcc ]; then
      TMPDIR=$(mktemp -d);
      cd $TMPDIR;
      wget https://hdf-wordpress-1.s3.amazonaws.com/wp-content/uploads/manual/HDF5/HDF5_1_10_7/src/hdf5-${HDF5_VER}.tar.bz2;
      tar xjf hdf5-${HDF5_VER}.tar.bz2;
      mkdir -p /home/build/athenapk/hdf5-${HDF5_VER}-gcc/serial /home/build/athenapk/hdf5-${HDF5_VER}-gcc/parallel;
      cd hdf5-${HDF5_VER};
      CC=gcc ./configure --prefix=/home/build/athenapk/hdf5-${HDF5_VER}-gcc/serial && make -j8 && make install && make clean;
      CC=mpicc ./configure --prefix=/home/build/athenapk/hdf5-${HDF5_VER}-gcc/parallel --enable-parallel && make -j8 && make install && make clean;
      cd;
      rm -rf $TMPDIR;
      fi
    - pip install unyt

cuda-regression:
  tags:
    - gpu
    - springdale7
  stage: regression
  script:
    - module load rh/devtoolset/8 cudatoolkit/11.1
    - mkdir build-cuda
    - cd build-cuda
    - cmake
      -DKokkos_ARCH_SKX=True
      -DKokkos_ENABLE_CUDA=True -DKokkos_ARCH_VOLTA70=True
      -DCMAKE_CXX_COMPILER=${PWD}/../external/Kokkos/bin/nvcc_wrapper
      -DPARTHENON_DISABLE_MPI=ON
      -DHDF5_ROOT=/home/build/athenapk/hdf5-${HDF5_VER}-gcc/serial
      ../
    - make -j${J} athenaPK
    - nvidia-smi
    - ctest
  artifacts:
    when: always
    expire_in: 3 days
    paths:
      - build-cuda/CMakeFiles/CMakeOutput.log
      - build-cuda/tst/regression/outputs/convergence
      - build-cuda/tst/regression/outputs/mhd_convergence
      - build-cuda/tst/regression/outputs/performance
      - build-cuda/tst/regression/outputs/cluster_hse/analytic_comparison.png
      - build-cuda/tst/regression/outputs/cluster_tabular_cooling/convergence.png
      - build-cuda/tst/regression/outputs/aniso_therm_cond_ring_conv/ring_convergence.png
      - build-cuda/tst/regression/outputs/field_loop/field_loop.png
